defer="true";


var count = (function () {
    var counter = 0;
    return function () {return counter += 1;}
})();

function isDate(myDate) {
    return myDate.constructor.toString().indexOf("Date") > -1;
} 


function isArray(myArray) {
    return myArray.constructor.toString().indexOf("Array") > -1;
}
